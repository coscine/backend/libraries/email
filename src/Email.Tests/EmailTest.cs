using NUnit.Framework;
using System;
using System.Threading.Tasks;
using Coscine.Configuration;

namespace Coscine.Email.Tests
{
    public class EmailTest
    {
        private static readonly IConfiguration _configuration = new ConsulConfiguration();

        private static readonly string _port = _configuration.GetStringAndWait("coscine/global/email/port");
        private static readonly string _host = _configuration.GetStringAndWait("coscine/global/email/host");
        private static readonly string _from = _configuration.GetStringAndWait("coscine/global/email/from");

        private static readonly string to = "westerhoff@itc.rwth-aachen.de";
        private static readonly string mailSubject = "Test Email";
        private static readonly string content = "Das ist eine Test Email.";

        
        [Test]
        public void SendEmailTest()
        {
            Email email = new Email(_port, _host, null, null);
            bool a = email.SendEmail(_from, to, null, mailSubject, content);
            email.Dispose();
            Assert.True(a);
        }

        [Test]
        public async Task SendEmailAsyncTest()
        {
            Email email = new Email(_port, _host, null, null);
            await email.SendEmailAsync(_from, to, null, mailSubject, content);
            email.Dispose();
            Assert.Pass();
        }

    }
}