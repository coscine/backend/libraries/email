﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using MimeKit.Text;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;

namespace Coscine.Email
{

    public class Email : IEmail, IDisposable
    {
        private readonly SmtpClient _client;

        public Email(string port, string host, string credentialName, string credentialPassword, bool ssl = false)
        {
            _client = new SmtpClient();
            
            bool parsed = int.TryParse(port, out int portTemp);
            if (!parsed)
            {
                throw new FormatException("Please check the provided port!");
            }
            _client.Connect(host, portTemp, ssl);
            if (!string.IsNullOrEmpty(credentialName) && !string.IsNullOrEmpty(credentialPassword))
            {
                _client.Authenticate(credentialName, credentialPassword);
            }            
        }

        public bool SendEmail(string from, string to, string cc, string mailSubject, Stream content)
        {
            return SendEmail(from, to, cc, mailSubject, StreamToString(content));
        }

        public bool SendEmail(string from, string to, string cc, string mailSubject, string content)
        {
            try
            {
                MimeMessage mail = CreateMailMessage(from, to, cc, mailSubject, content);
                _client.Send(mail);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task SendEmailAsync(string from, string to, string cc, string mailSubject, Stream content)
        {
            await SendEmailAsync(from, to, cc, mailSubject, StreamToString(content));
        }

        public async Task SendEmailAsync(string from, string to, string cc, string mailSubject, string content)
        {
            MimeMessage mail = CreateMailMessage(from, to, cc, mailSubject, content);
            await _client.SendAsync(mail);
        }

        private static string StreamToString(Stream content)
        {
            string s;
            using (var sr = new StreamReader(content))
            {
                s = sr.ReadToEnd();
            }
            return s;
        }

        private static MimeMessage CreateMailMessage(string from, string to, string cc, string mailSubject, string content)
        {
            MimeMessage email = new MimeMessage();

            email.From.Add(MailboxAddress.Parse(from));
            email.To.Add(MailboxAddress.Parse(to));
            email.Subject = HttpUtility.HtmlDecode(mailSubject);
            email.Body = new TextPart(TextFormat.Html) { Text = content };

            if (!string.IsNullOrEmpty(cc))
            {
                email.Cc.Add(MailboxAddress.Parse(cc));
            }

            return email;
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}
