﻿using System.IO;
using System.Threading.Tasks;

namespace Coscine.Email
{
    public interface IEmail
    {
        bool SendEmail(string from, string to, string cc, string mailSubject, Stream content);
        bool SendEmail(string from, string to, string cc, string mailSubject, string content);
        Task SendEmailAsync(string from, string to, string cc, string mailSubject, Stream content);
        Task SendEmailAsync(string from, string to, string cc, string mailSubject, string content);
    }
}
