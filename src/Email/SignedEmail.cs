﻿using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Cryptography;
using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Coscine.Email
{
    public class SignedEmail : IEmail, IDisposable
    {
        private readonly SmtpClient _client;
        private readonly string _certificatePath;
        private readonly string _certificatePassword;
        private readonly string _separator = ";";

        public SignedEmail(string certificatePath, string certificatePassword, string host, string port)
        {
            _certificatePath = certificatePath;
            _certificatePassword = certificatePassword;

            _client = new SmtpClient();
            int.TryParse(port, out int portTemp);
            _client.Connect(host, portTemp, false);
        }

        
        public bool SendEmail(string from, string to, string cc, string mailSubject, Stream content)
        {
            return SendEmail(from, to, cc, mailSubject, StreamToString(content));
        }

        public bool SendEmail(string from, string to, string cc, string mailSubject, string content)
        {
            try
            {
                MimeMessage message = CreateMailMessage(from, to.Split(_separator), cc, mailSubject);
                message = SignMailMessage(message, content, _certificatePath, _certificatePassword);

                _client.Send(message);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task SendEmailAsync(string from, string to, string cc, string mailSubject, Stream content)
        {
            await SendEmailAsync(from, to, cc, mailSubject, StreamToString(content));
        }

        public async Task SendEmailAsync(string from, string to, string cc, string mailSubject, string content)
        {
            MimeMessage message = CreateMailMessage(from, to.Split(_separator), cc, mailSubject);
            message = SignMailMessage(message, content, _certificatePath, _certificatePassword);

            await _client.SendAsync(message);
        }

        private static string StreamToString(Stream content)
        {
            string s;
            using (var sr = new StreamReader(content))
            {
                s = sr.ReadToEnd();
            }
            return s;
        }

        private static MimeMessage CreateMailMessage(string from, string[] to, string cc, string mailSubject)
        {
            MimeMessage message = new MimeMessage();
            message.From.Add(new MailboxAddress(from, from));

            for(int i = 0; i < to.Length; i++)
            {
                message.To.Add(new MailboxAddress(to[i], to[i]));
            }
            
            message.Subject = mailSubject;
            if (!string.IsNullOrEmpty(cc))
            {
                message.Cc.Add(new MailboxAddress(cc, cc));
            }

            return message;
        }

        private static MimeMessage SignMailMessage(MimeMessage message, string content, string certificatePath, string certificatePassword)
        {
            var body = new TextPart("html")
            {
                Text = content
            };

            var certificate = new X509Certificate2(certificatePath, certificatePassword, X509KeyStorageFlags.Exportable);

            using (var ctx = new TemporarySecureMimeContext())
            {
                CmsSigner signer = new CmsSigner(certificate);
                message.Body = ApplicationPkcs7Mime.Sign(ctx, signer, body);
            }

            return message;
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}
